# LORA for MeteoLORA

Dépôt du sous-projet LORA avec les documents fournis par les tuteurs ainsi que les productions des étudiants et le tableau de bord des tâches de l'équipe 

Objectif du projet : développer un système bout-en-bout de station météo, des capteurs de données météo qui transmettent en LoRaWAN au tableau de bord web.

## Documentation
La documentation fournie par les tuteurs est donnée dans le dossier Documentation.

## LoRaWAN introduction
 
[ Un tutoriel pour tout savoir en 40 min](https://www.youtube.com/watch?v=j0ONEdkOm28)

## GitLab

- [Un tutoriel pour bien utiliser Git Lab ](https://www.youtube.com/watch?v=-oaI2WEKdI4&list=PL05JrBw4t0KofkHq4GZJ05FnNGa11PQ4d)

- [Un autre pour parler Git ](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)

## Commencer à prendre en main le matériel Pycom

### Environnement de développement Pycom
Pycom propose des cartes de développement pour communiquer notamment en LoRaWAN avec l'opérateur The Things Network (TTN). 
Pour programmer les cartes, il faut
- [ ] un éditeur de code : VS Code -> à télécharger et installer sur votre machine
- [ ] une extension appelée Pymakr -> à télécharger dans les extensions de VS Code.

Tuto de Pycom sur l'installation de VS Code : https://docs.pycom.io/gettingstarted/software/vscode/
‼️ l'interface de Pymakr a changé depuis. En particulier la manière de se connecter à la carte et de la programmmer.
Se référer au [dépôt github](https://github.com/pycom/pymakr-vsc/) pour un readme plus précis.

Les codes utiles à ce projet sont dans le dossier [Examples](https://gitlab.imt-atlantique.fr/23-codev-meteolora/lora/-/tree/main/Examples)


### Tutoriels Pycom
Vous pouvez commencer par la prise en main des cartes LoPy4 à l'aide des [tutoriels présents sur le site internet du telefab](https://telefab.fr/2021/09/27/demarrer-avec-lopy4-pycom/)


(en option pour en savoir plus sur la couche physique de la technologie LoRa vous pouvez tester [ce tutoriel ](https://telefab.fr/2021/10/04/aller-plus-loin-avec-la-lopy4/))

Pour avoir les dernières releases de la pysense : https://github.com/pycom/pycom-libraries/releases/

La carte collaborative des GW TTN est ici : https://ttnmapper.org/heatmap/

#### Se connecter au serveur TTN
Pour la documentation pour se connecter sur le serveur TTN c'est ici :  https://telefab.fr/2021/10/18/mini-projet-capteur-meteo-et-lorawan-avec-la-lopy4/

Voici le le lien de connexion : https://eu1.cloud.thethings.network/console/

Le login et mdp sont donnés par l'encadrante.

#### Envoyer des données sous forme de bytes dans TTN
https://www.thethingsnetwork.org/docs/devices/bytes/#how-many-bytes-can-i-send

### Accès Fablab

La GW TTN est présente au fablab (à droite sur le mur en bas en entrant). Pour demander l'accès au fablab avec votre badge, c'est ici : https://moodle.imt-atlantique.fr/course/view.php?id=1470

Bien lire et signer la charte. Pour récupérer votre code, section Infos et Outils du cours Moodle Telefab.

## Contribuer à ttnmapper

Pour contribuer au [projet ttnmapper](https://ttnmapper.org/heatmap/), il faut pouvoir indiquer la localisation des noeuds (devices). Or, tous les noeuds ne possèdent pas de capacité de géolocalisation (chez Pycom, la carte PyTrack est la seule à posséder un GPS intégré). Pour cela, JP Meijers a créé une application smartphone (Android et iOS) pour coupler votre smartphone (qui lui possède des capacité de géolocalisation) avec un noeud particulier. À tester !

Plus d'infos ici : 

https://www.thethingsnetwork.org/labs/story/using-ttnmapper-on-android

https://docs.ttnmapper.org/FAQ.html


---
---
---

## Getting started with GitLab

How to clone the repo on your computer
In a terminal

```
mkdir MeteoLORA
cd MeteoLORA
git clone https://gitlab.imt-atlantique.fr/23-codev-meteolora/lora.git
```


## Add your files in your main remote branch

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) 

```
git add <name of file> 
``` 
or
```
git add *
``` 
```
git commit -m "DESCRIBE COMMIT IN A FEW WORDS"
git push -uf origin main
```

## Add your files in a new branch (recommended if you are working in a team) 

```
git checkout -b example-tutorial-branch % create a new local branch
git status
git add <name of file> 
git commit -m "DESCRIBE COMMIT IN A FEW WORDS"
git push origin example-tutorial-branch % add a remote branch
```
Your branch is now available on GitLab and visible to other users in your project.

## Make your fist git commit

https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html#create-a-branch-and-make-changes


## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
