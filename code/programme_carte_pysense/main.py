from network import LoRa
import socket
import time
import struct
import ubinascii
import binascii
import pycom
import machine
import utime


from LIS2HH12 import LIS2HH12
from SI7006A20 import SI7006A20
from LTR329ALS01 import LTR329ALS01
from MPL3115A2 import MPL3115A2,ALTITUDE,PRESSURE
from pycoproc_2 import Pycoproc

###################################### DEFINITION DES FONCTIONS #################################################

def connect_to_ttn(lora_object):
    """Receives a lora object and tries to join"""
    # join a network using OTAA (Over the Air Activation)
    lora_object.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
    #lora_object.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0, dr=0)
    pycom.rgbled(0xff00ff)
    # wait until the module has joined the network
    while not lora_object.has_joined():
        #pass
        time.sleep(5)
        print('Not yet joined...')
        lora.nvram_erase()


####################################### CODE EXECUTE PAR LA CARTE ############################################# 

pycom.heartbeat(False)
py = Pycoproc() # Initialise l'objet Pycoproc pour accéder aux fonctionnalités de la carte Pycom

# Initialiation des capteurs pour effectuer les mesures
press = MPL3115A2(py,mode=PRESSURE)
dht = SI7006A20(py)
li = LTR329ALS01(py)

# Définition des paramètres de la carte
app_eui = ubinascii.unhexlify('0000000000000000')
app_key = ubinascii.unhexlify('696DC833A0EDA629C21CD4D358F42757')
dev_eui = ubinascii.unhexlify('70B3D5499A66E878')

# Défiition des paramètres de communication LoRa
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, adr=True, device_class = LoRa.CLASS_C)
lora.nvram_restore() #if there is nothing to restore it will return a False

print(binascii.hexlify(lora.mac()).upper()) # Affichage de l'adresse MAC de la carte

# Connexion au serveur TTN et changement de la couleur de la LED en vert
connect_to_ttn(lora)
print("CONNECTED!!")
pycom.rgbled(0x00ff00)

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# make the socket blocking
s.setblocking(True)


while True: # Boucle infinie permettant de récolter les données et de les envoyer sur TTN
    # Stockage des données mesurées dans des variables 
    temp = float(str(dht.temperature()))
    temp_arrondi = round(temp,1)
    humidity=float(str(dht.humidity()))
    humidity_arrondi=round(humidity)
    pression=float(str(press.pressure()))
    pression_arrondi=round(pression)
    lumin=float(str(li.lux()))
    lumin_arrondi=round(lumin)

    # Envoi des données à TTN sous la forme d'une seule chaîne de caractères, séparées par des virgules
    s.send( str(temp_arrondi) +','+ str(humidity_arrondi) + ',' + str(pression_arrondi) +',' + str(lumin_arrondi))

    # Envoi des données suivantes après 5 minutes
    time.sleep(300)