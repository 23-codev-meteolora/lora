# Code du projet CODEV MétéoLora

Dépot du code du projet et explications pour son implémentation sur un ordinateur fonctionnant sous Windows.

## Programmes de la carte PyCom
Les programmes à envoyer dans la carte Pycom se trouvent dans le dossier *programme_carte_pysense*.


Le dossier *lib* contient les fonctions concernant les différents capteurs, tandis que le code executé par la carte se trouve dans *main.py*. Les données sont ainsi envoyées sur le serveur TTN.

## Programmes pour l'ordinateur où se trouve l'interface
Les programmes pour faire fonctionner l'interface se trouvent dans le dossier *interface*.

Le fichier *index.js* est le fichier executé par Node permettant d'implémenter le client MQTT. Les données envoyées par TTN sont récupérées. Elles remplacent les plus récentes dans le fichier *data.json* et sont stockées dans le fichier *historique.csv* (celui-ci est créé s'il n'existe pas encore).
Le fichier *package.json* est nécéssaire pour lancer le code.

Les fichiers *index.html*, *interface.css* et *display.js* concernent directement l'interface. Les deux premiers implémentent son design, tandis que *display.js* est exécuté pour récupérer les données dans le fichier *data.json* et les afficher.

## Comment faire fonctionner les codes ? (Windows 10)
### 1) Programmer la carte Pysense
Il suffit simplement d'envoyer le contenu du dossier *programme_carte_pysense* en branchant la carte à l'ordinateur. Une solution possible est d'utiliser l'extension PyMakr de Visual Studio Code.

### 2) Faire fonctionner le programme Node
Télécharger Node.js

Ouvrir l'invité de commande Windows et se placer dans le répértoire interface_web. Exemple : *cd Documents/interface*

Ecrire la ligne de commande *npm start* pour executer le code *index.js*

### 3) Faire fonctionner l'interface
Télécharger Google Chrome et installer l'extension *Web Server for Chrome*

Spécifier le chemin du dossier *interface* pour l'importer dans l'extension.

Relever l'URL du Web Serveur indiqué et modifier la ligne 35 du code *display.js* en conséquence.

Cliquer alors sur *index.html* dans le Web Server pour ouvrir l'interface.

