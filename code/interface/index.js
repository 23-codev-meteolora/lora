const mqtt = require('mqtt');
const fs = require('fs');

//-----------------------------------------DEFINITION DES FONCTIONS--------------------------------------------//

function extractTemp(str) { // Fonction permettant d'extraire la donnée se trouvant avant la 1ère virgule dans le paquet
  const index = str.indexOf(','); // Recherche l'indice de la première occurrence de la virgule dans la chaîne de caractères
  if (index !== -1) { // Si la virgule est trouvée dans la chaîne
    return str.slice(0, index); // Retourne une sous-chaîne à partir du début jusqu'à l'indice de la virgule (exclus)
  } else {
    return null;
  }
}

function extractHumidity(str) { // Fonction permettant d'extraire la donnée se trouvant après la 2ème virgule dans le paquet
  const index1 = str.indexOf(',');
  const index2 = str.indexOf(',', index1 + 1);
  if (index1 !== -1 && index2 !== -1) {
    return str.slice(index1 + 1, index2);
  } else {
    return null;
  }
}

function extractPressure(str) { // Fonction permettant d'extraire la donnée se trouvant après la 3eme virgule dans le paquet
  const index1 = str.indexOf(',');
  const index2 = str.indexOf(',', index1 + 1);
  const index3 = str.indexOf(',', index2 + 1);
  if (index2 !== -1 && index3 !== -1) {
    return str.slice(index2 + 1, index3);
  } else {
    return null;
  }
}

function extractLuminosity(str) { // Fonction permettant d'extraire la donnée se trouvant avant la 4eme virgule dans le paquet
  const index1 = str.indexOf(',');
  const index2 = str.indexOf(',', index1 + 1);
  const index3 = str.indexOf(',', index2 + 1);
  if (index3 !== -1) {
    return str.slice(index3 + 1);
  } else {
    return null;
  }
}

function extractTimeFromMessage(message) { // Fonction prenant en argument le message complet provenant de TTN, et renvoyant l'heure du message
  const regex = /(\d{2}):(\d{2}):(\d{2})/; 
  const match = regex.exec(message); // Recherche l'heure dans le message
  if (match) { // Si une correspondance est trouvée
    const hour = match[1]; // Récupère l'heure du message
    const minute = match[2]; // Récupère la minute
    
    // Crée un objet Date pour l'heure extraite du message
    const messageTime = new Date();
    messageTime.setHours(hour);
    messageTime.setMinutes(minute);
    messageTime.setSeconds(0);
    
    // Ajoute 2 heures à l'objet Date pour la mettre dans le bon fuseau horaire
    const newTime = new Date(messageTime.getTime() + 2 * 60 * 60 * 1000);
    
    // Extraire l'heure et les minutes de l'objet modifié
    const newHour = newTime.getHours();
    const newMinute = newTime.getMinutes();
    
    // Formate l'heure et les minutes dans une chaîne HH:MM
    return `${newHour.toString().padStart(2, '0')}:${newMinute.toString().padStart(2, '0')}`;
  } else {
    return "No time found in message";
  }
}

function extractDateFromMessage(message) { // Fonction prenant en argument le message complet provenant de TTN, et renvoyant la date du message
  const regex = /"received_at":"(\d{4})-(\d{2})-(\d{2})T/;
  const match = message.match(regex); // Recherche la date dans le message

  if (match) { // Si une correspondance est trouvée
    const year = match[1]; // récupère l'année du message
    const month = match[2]; // récupère le mois du message
    const day = match[3]; // récupère le jour du message

    // Construit la date sous le format JJ/MM/AAAA
    const date = `${day}/${month}/${year}`;
    return date;
  } else {
    return 'Aucune date trouvée dans le message.';
  }
}

function dataToCSV(donnees) { // Prend en argument un dictionnaire de données et le convertit en un fichier historique.csv
    let csvHeader = '';
    if (!fs.existsSync('historique.csv')) { // Si le fichier n'existe pas, alors on initialise le nom des colonnes
      csvHeader = 'Jour,Heure,Temperature,Humidite,Luminosite,Pression\n';
    }
    // Convertit les données en format CSV
    let csvData = Object.values(donnees).join(',') + '\n';

    // Enregistre les données dans un fichier CSV
    fs.appendFile('historique.csv', csvHeader + csvData, function (err) {
      if (err) throw err;
      console.log('Les données ont été enregistrées dans le fichier historique.csv'); // indique sur la console que l'enregistrement des données a eu lieu
    });

}

// ----------------------------------------------------CODE EXECUTE PAR NODE-----------------------------------//

const host = 'eu1.cloud.thethings.network' // Adresse du serveur TTN
const port = '1883' // Port utilisé par TTN
const clientId = `mqtt_${Math.random().toString(16).slice(3)}`

const connectUrl = `mqtt://${host}:${port}`

const client = mqtt.connect(connectUrl, { // connexion au serveur TTN
  clientId,
  clean: true,
  connectTimeout: 4000,
  username: 'tp-lora-cooc@ttn',
  password: 'NNSXS.I6TFTQ5RRVWM242KRQTBTKYSXZ6HTPUBK6MLGIY.UXTRE5SCGUS6GRAG4YBCQ3FHC7745HYNHPHIEZLBX26Y5T6JCYVQ',
  reconnectPeriod: 1000,
})

const topic = 'v3/tp-lora-cooc@ttn/devices/eui-70b3d5499a66e878/up' // topic sur lequel TTN publie les données
client.on('connect', () => { // se déclenche lorsque le client MQTT est connecté
  console.log('Connected') 
  client.subscribe([topic], () => { // Abonnement au topic
    console.log(`Subscribe to topic '${topic}'`)
  })
})
client.on('message', (topic, payload) => { // se déclenche lorsqu'un message est reçu
  // Recherche et extraction du frm_payload dans le message envoyé par TTN
  const message = payload.toString()
  const payloadRegex = /"frm_payload":"(.+?)"/; 
  const payloadMatch = message.match(payloadRegex); 

  // Si le frm_payload est trouvé, alors les données sont extraites et stockées dans différentes variables
  if (payloadMatch) { 
    const frm_payload = payloadMatch[1];
    const temp = extractTemp(atob(frm_payload));
    const pres = extractPressure(atob(frm_payload));
    const hum = extractHumidity(atob(frm_payload));
    const lum = extractLuminosity(atob(frm_payload));
    const date = extractDateFromMessage(message)
    const heure = extractTimeFromMessage(message)

    // Stockage de ces données dans un dictionnaire
    let donnees = {
      "date" : date,
      "heure": heure,
      "temperature" : temp,
      "pression" : pres,
      "humidite" : hum,
      "luminosite" : lum
   }
   // Ecriture de ces données dans le fichier JSON
   let data = JSON.stringify(donnees)
   fs.writeFileSync('data.json', data)

   // Mise à jour du fichier CSV
   dataToCSV(donnees)

   // Affichage des données dans la console Node
    console.log(atob(frm_payload));
    console.log(date)
    console.log(heure)
} else {
    console.log("frm_payload non trouvé dans le message.");
}

})



