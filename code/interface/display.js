setInterval(function() { //actualisation de la page toutes les 5 minutes
	location.reload();
  }, 300000);

 //------------------------------------------DEFINITION DES FONCTIONS-------------------------------------------// 
function estAujourdhui(date) {
	let maintenant = new Date(); // Date et heure actuelles
	let [jour, mois, annee] = date.split("/"); // Récupère le jour, le mois et l'année de la chaîne de caractères
  
	let dateCompare = new Date(annee, mois - 1, jour); // Initialise une nouvelle date avec l'année, le mois et le jour de la chaîne de caractères
  
	return (dateCompare.toDateString() === maintenant.toDateString()); // Renvoie true si les deux dates sont les mêmes
  }

function estProcheDeMaintenant(heure) {
	let maintenant = new Date(); // Date et heure actuelles
	let [heures, minutes] = heure.split(":"); // Récupère les heures et les minutes de la chaîne de caractères
  
	let dateCompare = new Date(); // Initialise une nouvelle date avec la même date que maintenant
	dateCompare.setHours(heures, minutes, 0, 0); // Met à jour les heures et les minutes, et met les secondes et les millisecondes à 0
  
	let diffEnMinutes = Math.floor((maintenant - dateCompare) / (1000 * 60)); // Calcule la différence en minutes
  
	return (diffEnMinutes < 10); // Renvoie true si l'écart est inférieur à 10 minutes
}
function pascalVersKiloPascal(pression) {
	let pascal = parseFloat(pression); // Convertit la chaîne de caractères en nombre
	let kiloPascal = pascal / 1000; // Convertit les pascals en kilopascals
	return Math.round(kiloPascal); // Renvoie le résultat arrondi à l'entier le plus proche
  }

//----------------------------------------CODE EXECUTE PAR LE NAVIGATEUR---------------------------------------//

// Effectue une requête HTTP pour obtenir les données du fichier JSON
fetch("http://127.0.0.1:8887/data.json")
	.then(function(response){
	   return response.json(); // Renvoie les données au format JSON
	})

	.then(function(data) { // Récupère les différentes valeurs des données
		let day = `${data.date}`;
		let hour = ` ${data.heure} `;
		let temp = ` ${data.temperature}°C `;
        let pressure=` ${data.pression}`;
        let humidity=` ${data.humidite}% `;
        let luminosity=` ${data.luminosite}Lux `;
        
		// Met à jour les éléments HTML avec les valeurs récupérées
		document.getElementById("day").innerHTML = day;
		document.getElementById("hour").innerHTML = hour;
		document.getElementById("temp").innerHTML = temp;
        document.getElementById("pres").innerHTML = pascalVersKiloPascal(pressure) + "kPa";
        document.getElementById("hum").innerHTML = humidity;
        document.getElementById("lum").innerHTML = luminosity;
		
		// Vérifie les conditions pour afficher l'état
		if (!estAujourdhui(day)) {
			document.getElementById("etat").innerHTML = "indisponible";
		}
		else if (estProcheDeMaintenant(hour)) {
			document.getElementById("etat").innerHTML =  "en marche";
		}
		else if (!estProcheDeMaintenant(hour)) {
			document.getElementById("etat").innerHTML =  "indisponible";
		}
	  })