# examples from https://docs.pycom.io/tutorials/networks/lora/module-module/

from network import LoRa
import socket
import time

# Please pick the region that matches where you are using the device

lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868)
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
#lora.sf(8)
#lora.coding_rate(lora.CODING_4_8)

s.setblocking(False)

msg = 'Ping'
#msg = 'Ping hello i m devui' + (ubinascii.hexlify(lora.mac()).decode('ascii'))
#print(msg)
i = 0
while True:
    s.send(msg)
    print('Ping {}'.format(i))
    print(lora.stats())
    i= i+1
    time.sleep(5)
